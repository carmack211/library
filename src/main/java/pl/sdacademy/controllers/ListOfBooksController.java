package pl.sdacademy.controllers;

import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.sdacademy.modelFx.AuthorFx;
import pl.sdacademy.modelFx.BookFx;
import pl.sdacademy.modelFx.CategoryFx;
import pl.sdacademy.modelFx.ListOfBooksModel;
import pl.sdacademy.utils.DialogsUtils;
import pl.sdacademy.utils.FxmlUtils;
import pl.sdacademy.utils.exceptions.ApplicationException;

import java.io.IOException;
import java.time.LocalDate;

public class ListOfBooksController {
    @FXML
    private TableView<BookFx> booksTableView;
    @FXML
    private TableColumn<BookFx, String> titleColumn;
    @FXML
    private TableColumn<BookFx, String> descriptionColumn;
    @FXML
    private TableColumn<BookFx, AuthorFx> authorColumn;
    @FXML
    private TableColumn<BookFx, CategoryFx> categoryColumn;
    @FXML
    private TableColumn<BookFx, Number> ratingColumn;
    @FXML
    private TableColumn<BookFx, String> isbnColumn;
    @FXML
    private TableColumn<BookFx, LocalDate> releaseDateColumn;
    @FXML
    private TableColumn<BookFx, BookFx> deleteColumn;
    @FXML
    private TableColumn<BookFx, BookFx> editColumn;

    private ListOfBooksModel listOfBooksModel;

    @FXML
    private ComboBox categoryComboBox;
    @FXML
    private ComboBox authorComboBox;

    @FXML
    public void initialize(){
        this.listOfBooksModel = new ListOfBooksModel();
        try {
            listOfBooksModel.init();
        } catch (ApplicationException e) {
            DialogsUtils.dialogError(e.getMessage());
        }
        this.categoryComboBox.setItems(this.listOfBooksModel.getCategoryFxObservableList());
        this.authorComboBox.setItems(this.listOfBooksModel.getAuthorFxObservableList());
        this.listOfBooksModel.categoryFxObjectPropertyProperty().bind(this.categoryComboBox.valueProperty());
        this.listOfBooksModel.authorFxObjectPropertyProperty().bind(this.authorComboBox.valueProperty());

        this.booksTableView.setItems(this.listOfBooksModel.getBookFxObservableList());
        this.titleColumn.setCellValueFactory(cellValue -> cellValue.getValue().titleProperty());
        this.descriptionColumn.setCellValueFactory(cellValue -> cellValue.getValue().descriptionProperty());
        this.ratingColumn.setCellValueFactory(cellValue -> cellValue.getValue().ratingProperty());
        this.isbnColumn.setCellValueFactory(cellValue -> cellValue.getValue().isbnProperty());
        this.releaseDateColumn.setCellValueFactory(cellValue->cellValue.getValue().releaseDateProperty());
        this.authorColumn.setCellValueFactory(cellValue->cellValue.getValue().authorFxProperty());
        this.categoryColumn.setCellValueFactory(cellValue->cellValue.getValue().categoryFxProperty());
        this.deleteColumn.setCellValueFactory(cellValue->new SimpleObjectProperty<>(cellValue.getValue()));
        this.editColumn.setCellValueFactory(cellValue->new SimpleObjectProperty<>(cellValue.getValue()));

        this.deleteColumn.setCellFactory(param -> new TableCell<BookFx, BookFx>(){
            Button button = createButton("/pl/sdacademy/icons/clear.png");
            @Override
            protected void updateItem(BookFx item, boolean empty) {
                super.updateItem(item, empty);
                if(empty){
                    setGraphic(null);
                }

                if(!empty){
                    setGraphic(button);
                    button.setOnAction(event -> {
                        try {
                            listOfBooksModel.deleteBook(item);
                        } catch (ApplicationException e) {
                            DialogsUtils.dialogError(e.getMessage());
                        }
                    });
                }
            }
        });

        this.editColumn.setCellFactory(param -> new TableCell<BookFx, BookFx>() {
            Button button = createButton("/pl/sdacademy/icons/edit.png");
            @Override
            protected void updateItem(BookFx item, boolean empty) {
                super.updateItem(item, empty);
                if(empty){
                    setGraphic(null);
                }
                if(!empty){
                    setGraphic(button);
                    button.setOnAction(event -> {
                        FXMLLoader loader = FxmlUtils.getLoader("/pl/sdacademy/fxml/AddBook.fxml");
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load());
                        } catch (IOException e) {
                            DialogsUtils.dialogError(e.getMessage());
                        }
                        BookController controller = loader.getController();
                        controller.getBookModel().setBookFxObjectProperty(item);
                        controller.bindings();
                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.showAndWait();
                    });
                }
            }
        });

    }

    private Button createButton(String path) {
        Button button = new Button();
        Image image = new Image(this.getClass().getResource(path).toString());
        ImageView imageView = new ImageView(image);
        button.setGraphic(imageView);
        return button;
    }

    public void filterDataComboBox() {
        this.listOfBooksModel.filterListOfBooks();
    }

    public void clearCategoryComboBox() {
        this.categoryComboBox.getSelectionModel().clearSelection();
    }

    public void clearAuthorComboBox() {
        this.authorComboBox.getSelectionModel().clearSelection();
    }
}
