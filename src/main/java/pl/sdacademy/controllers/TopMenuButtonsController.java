package pl.sdacademy.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ToggleGroup;

/**
 * Created by Grzegorz on 2017-05-22.
 */
public class TopMenuButtonsController {

    public static final String LIBRARY_FXML = "/pl/sdacademy/fxml/Library.fxml";
    public static final String LIST_OF_BOOKS_FXML = "/pl/sdacademy/fxml/ListOfBooks.fxml";
    public static final String STATISTICS_FXML = "/pl/sdacademy/fxml/Statistics.fxml";
    public static final String ADD_BOOK_FXML = "/pl/sdacademy/fxml/AddBook.fxml";
    private static final String ADD_CATEGORY_FXML = "/pl/sdacademy/fxml/AddCategory.fxml";
    private static final String ADD_AUTHOR_FXML = "/pl/sdacademy/fxml/AddAuthor.fxml";

    private MainController mainController;

    @FXML
    private ToggleGroup toggleButtons;

    @FXML
    public void openLibrary() {
        mainController.setCenter(LIBRARY_FXML);
    }

    @FXML
    public void openListOfBooks() {
        mainController.setCenter(LIST_OF_BOOKS_FXML);
    }

    @FXML
    public void openStatistics() {
        mainController.setCenter(STATISTICS_FXML);
    }

    @FXML
    public void addBook() {
        resetToggleButton();
        mainController.setCenter(ADD_BOOK_FXML);
    }

    @FXML
    public void addCategory() {
        resetToggleButton();
        mainController.setCenter(ADD_CATEGORY_FXML);
    }

    @FXML
    public void addAuthor(){
        resetToggleButton();
        mainController.setCenter(ADD_AUTHOR_FXML);
    }

    private void resetToggleButton() {
        if(toggleButtons.getSelectedToggle()!=null) {
            toggleButtons.getSelectedToggle().setSelected(false);
        }
    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

}
