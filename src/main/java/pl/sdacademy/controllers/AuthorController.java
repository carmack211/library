package pl.sdacademy.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import pl.sdacademy.modelFx.AuthorFx;
import pl.sdacademy.modelFx.AuthorModel;
import pl.sdacademy.utils.DialogsUtils;
import pl.sdacademy.utils.exceptions.ApplicationException;

import java.sql.SQLException;

public class AuthorController {

    @FXML
    private TextField authorNameTextField;

    @FXML
    private TextField authorSurnameTextField;

    @FXML
    private Button authorAddButton;

    @FXML
    private TableView<AuthorFx> authorTableView;

    @FXML
    private TableColumn<AuthorFx, String> nameColumn;

    @FXML
    private TableColumn<AuthorFx, String> surnameColumn;

    @FXML
    private MenuItem deleteMenuItem;

    private AuthorModel authorModel;

    public void initialize(){
        this.authorModel = new AuthorModel();
        try {
            this.authorModel.init();
        } catch (ApplicationException e) {
            DialogsUtils.dialogError(e.getMessage());
        }

        bindings();
        bindingsTableView();

    }

    private void bindings() {
        this.authorModel.authorFxObjectPropertyProperty().
                get().nameProperty().bind(this.authorNameTextField.textProperty());
        this.authorModel.authorFxObjectPropertyProperty().
                get().surnameProperty().bind(this.authorSurnameTextField.textProperty());
        this.authorAddButton.disableProperty().
                bind(this.authorNameTextField.textProperty().isEmpty().
                        or(this.authorSurnameTextField.textProperty().isEmpty()));

        this.deleteMenuItem.disableProperty().bind(authorTableView.getSelectionModel().selectedItemProperty().isNull());
    }

    private void bindingsTableView() {
        this.authorTableView.setItems(this.authorModel.getAuthorFxObservableList());
        this.nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        this.surnameColumn.setCellValueFactory(cellData -> cellData.getValue().surnameProperty());

        //ustawiamy możliwość edytowania komórki (2x lpm)
        this.nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        this.surnameColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        this.authorTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            this.authorModel.setAuthorFxObjectPropertyEdit(newValue);
        });
    }

    public void addAuthor() {
        try {
            this.authorModel.saveAuthorInDataBase();
        } catch (ApplicationException e) {
            DialogsUtils.dialogError(e.getMessage());
        }
        this.authorNameTextField.clear();
        this.authorSurnameTextField.clear();
    }

    public void onEditCommitName(TableColumn.CellEditEvent<AuthorFx, String> authorFxStringCellEditEvent) {
        this.authorModel.getAuthorFxObjectPropertyEdit().setName(authorFxStringCellEditEvent.getNewValue());
        updateInDatabase();
    }

    public void onEditCommitSurname(TableColumn.CellEditEvent<AuthorFx, String> authorFxStringCellEditEvent) {
        this.authorModel.getAuthorFxObjectPropertyEdit().setSurname(authorFxStringCellEditEvent.getNewValue());
        updateInDatabase();
    }

    private void updateInDatabase() {
        try {
            this.authorModel.saveAuthorEditInDataBase();
        } catch (ApplicationException e) {
            DialogsUtils.dialogError(e.getMessage());
        }
    }

    public void deleteAuthor() {
        try {
            this.authorModel.deleteAuthorInDataBase();
        } catch (ApplicationException | SQLException e) {
            DialogsUtils.dialogError(e.getMessage());
        }
    }
}
