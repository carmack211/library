package pl.sdacademy.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import pl.sdacademy.modelFx.AuthorFx;
import pl.sdacademy.modelFx.BookModel;
import pl.sdacademy.modelFx.CategoryFx;
import pl.sdacademy.utils.DialogsUtils;
import pl.sdacademy.utils.exceptions.ApplicationException;

public class BookController {
    
    @FXML
    private ComboBox<CategoryFx> categoryComboBox;
    @FXML
    private ComboBox<AuthorFx> authorComboBox;
    @FXML
    private TextArea descriptionTextArea;
    @FXML
    private Slider ratingSlider;
    @FXML
    private TextField isbnTextField;
    @FXML
    private DatePicker releaseDatePicker;
    @FXML
    private TextField titleTextField;
    @FXML
    private Button addBookButton;

    private BookModel bookModel;

    public void initialize(){
        this.bookModel = new BookModel();
        try {
            this.bookModel.init();
        } catch (ApplicationException e) {
            DialogsUtils.dialogError(e.getMessage());
        }
        bindings();
        validation();
    }

    private void validation() {
        this.addBookButton.disableProperty().bind(this.categoryComboBox.valueProperty().isNull()
                .or(this.authorComboBox.valueProperty().isNull()
                        .or( this.titleTextField.textProperty().isEmpty()
                                .or(this.descriptionTextArea.textProperty().isEmpty()
                                        .or(this.isbnTextField.textProperty().isEmpty()
                                                .or(this.releaseDatePicker.valueProperty().isNull()))))));
    }

    public void bindings() {
        this.categoryComboBox.setItems(this.bookModel.getCategoryFxObservableList());
        this.authorComboBox.setItems(this.bookModel.getAuthorFxObservableList());
        //this.bookModel.getBookFxObjectProperty().categoryFxProperty().bind(this.categoryComboBox.valueProperty());
        //this.bookModel.getBookFxObjectProperty().authorFxProperty().bind(this.authorComboBox.valueProperty());
        //this.bookModel.getBookFxObjectProperty().titleProperty().bind(this.titleTextField.textProperty());
        //this.bookModel.getBookFxObjectProperty().descriptionProperty().bind(this.descriptionTextArea.textProperty());
        //this.bookModel.getBookFxObjectProperty().ratingProperty().bind(this.ratingSlider.valueProperty());
        //this.bookModel.getBookFxObjectProperty().isbnProperty().bind(this.isbnTextField.textProperty());
        //this.bookModel.getBookFxObjectProperty().releaseDateProperty().bind(this.releaseDatePicker.valueProperty());

        this.categoryComboBox.valueProperty().bindBidirectional(this.bookModel.getBookFxObjectProperty().categoryFxProperty());
        this.authorComboBox.valueProperty().bindBidirectional(this.bookModel.getBookFxObjectProperty().authorFxProperty());
        this.titleTextField.textProperty().bindBidirectional(this.bookModel.getBookFxObjectProperty().titleProperty());
        this.descriptionTextArea.textProperty().bindBidirectional(this.bookModel.getBookFxObjectProperty().descriptionProperty());
        this.ratingSlider.valueProperty().bindBidirectional(this.bookModel.getBookFxObjectProperty().ratingProperty());
        this.isbnTextField.textProperty().bindBidirectional(this.bookModel.getBookFxObjectProperty().isbnProperty());
        this.releaseDatePicker.valueProperty().bindBidirectional(this.bookModel.getBookFxObjectProperty().releaseDateProperty());
    }

    public void addBook() {
        try {
            this.bookModel.saveBookInDatabase();
            //clearView();
        } catch (ApplicationException e) {
            DialogsUtils.dialogError(e.getMessage());
        }

    }

    public void clearView(){
        this.categoryComboBox.getSelectionModel().clearSelection();
        this.authorComboBox.getSelectionModel().clearSelection();
        this.titleTextField.clear();
        this.descriptionTextArea.clear();
        this.ratingSlider.setValue(1);
        this.isbnTextField.clear();
        this.releaseDatePicker.getEditor().clear();
    }

    public BookModel getBookModel() {
        return bookModel;
    }
}
