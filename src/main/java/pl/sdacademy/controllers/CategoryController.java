package pl.sdacademy.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import pl.sdacademy.modelFx.CategoryFx;
import pl.sdacademy.modelFx.CategoryModel;
import pl.sdacademy.utils.DialogsUtils;
import pl.sdacademy.utils.exceptions.ApplicationException;

import java.sql.SQLException;

/**
 * Created by Grzegorz on 2017-07-04.
 */
public class CategoryController {

    @FXML
    private Button categoryAddButton;

    @FXML
    private Button categoryDeleteButton;

    @FXML
    private Button categoryEditButton;

    @FXML
    private TextField categoryTextField;

    @FXML
    private TreeView<String> categoryTreeView;

    @FXML
    private ComboBox<CategoryFx> categoryComboBox;

    private CategoryModel categoryModel;

    @FXML
    public void initialize(){
        this.categoryModel = new CategoryModel();
        try {
            this.categoryModel.init();
        } catch (ApplicationException e) {
            DialogsUtils.dialogError(e.getMessage());
        }
        this.categoryComboBox.setItems(this.categoryModel.getCategoryList());
        this.categoryTreeView.setRoot(this.categoryModel.getRoot());
        initButtons();
    }

    public void initButtons() {
        categoryAddButton.disableProperty().bind(categoryTextField.textProperty().isEmpty());
        categoryDeleteButton.disableProperty().bind(categoryModel.categoryProperty().isNull());
        categoryEditButton.disableProperty().bind(categoryModel.categoryProperty().isNull());
    }

    public void addCategory() {
        try {
            categoryModel.saveCategoryInDataBase(categoryTextField.getText());
        } catch (ApplicationException e) {
            DialogsUtils.dialogError(e.getMessage());
        }
        categoryTextField.clear();
    }

    public void deleteCategory() {
        try {
            categoryModel.deleteCategoryById();
        } catch (ApplicationException | SQLException e) {
            DialogsUtils.dialogError(e.getMessage());
        }
    }

    public void categoryComboBox() {
        this.categoryModel.setCategory
                (this.categoryComboBox.getSelectionModel().getSelectedItem());
    }

    public void editCategory() {
        String newCategoryValue = DialogsUtils.
                editDialog(this.categoryModel.getCategory().getName());
        if(newCategoryValue!=null){
            this.categoryModel.getCategory().setName(newCategoryValue);
            try {
                this.categoryModel.updateCategoryInDataBase();
            } catch (ApplicationException e) {
                DialogsUtils.dialogError(e.getMessage());
            }
        }
    }
}
