package pl.sdacademy.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;

import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by Grzegorz on 2017-05-24.
 */
public class DialogsUtils {

    private static ResourceBundle bundle = FxmlUtils.getResourcesBundle();

    public static void dialogAboutApplication(){
        Alert alertInformationAboutApp = new Alert(Alert.AlertType.INFORMATION);
        alertInformationAboutApp.setTitle(bundle.getString("menuBar.help.aboutApp"));
        alertInformationAboutApp.setHeaderText(bundle.getString("menuBar.help.aboutApp.header"));
        alertInformationAboutApp.setContentText(bundle.getString("menuBar.help.aboutApp.content"));
        alertInformationAboutApp.showAndWait();
    }

    public static Optional<ButtonType> dialogConfirmation(){
        Alert alertExitConfirmation = new Alert(Alert.AlertType.CONFIRMATION);
        alertExitConfirmation.setTitle(bundle.getString("menuBar.file.closeApp.confirmationTitle"));
        alertExitConfirmation.setHeaderText(bundle.getString("menuBar.file.closeApp.confirmationContent"));
        Optional<ButtonType> result = alertExitConfirmation.showAndWait();
        return result;
    }

    public static void dialogError(String error){
        Alert alertError = new Alert(Alert.AlertType.ERROR);
        alertError.setTitle(bundle.getString("alert.error.tilte"));
        alertError.setHeaderText(bundle.getString("alert.error.headerText"));

        TextArea textArea = new TextArea(error);
        alertError.getDialogPane().setContent(textArea);

        alertError.showAndWait();
    }

    public static String editDialog(String value) {
        TextInputDialog dialog = new TextInputDialog(value);
        dialog.setTitle(bundle.getString("dialog.edit.tilte"));
        dialog.setHeaderText(bundle.getString("dialog.edit.headerText"));
        dialog.setContentText(bundle.getString("dialog.edit.content"));
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()){
            return result.get();
        }
        return null;
    }


}
