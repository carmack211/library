package pl.sdacademy.utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

import java.util.ResourceBundle;

/**
 * Created by Grzegorz on 2017-05-24.
 */
public class FxmlUtils {


    public static Pane fxmlLoader(String fxmlPath){
        FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getClass().getResource(fxmlPath));
                loader.setResources(getResourcesBundle());
        try {
            return loader.load();
        } catch (Exception e) {
            DialogsUtils.dialogError(e.getMessage());
        }
        return null;
    }

    public static FXMLLoader getLoader(String fxmlPath){
        FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getClass().getResource(fxmlPath));
        loader.setResources(getResourcesBundle());
        return loader;
    }


    public static ResourceBundle getResourcesBundle() {
        return ResourceBundle.getBundle("pl.sdacademy.bundles.messages");
    }

}
