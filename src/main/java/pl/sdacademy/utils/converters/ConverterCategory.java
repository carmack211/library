package pl.sdacademy.utils.converters;

import pl.sdacademy.database.models.Category;
import pl.sdacademy.modelFx.CategoryFx;

public class ConverterCategory {

    public static CategoryFx convertToCategoryFx(Category category){
        CategoryFx categoryFx = new CategoryFx();
        categoryFx.setId(category.getId());
        categoryFx.setName(category.getName());
        return categoryFx;
    }
}
