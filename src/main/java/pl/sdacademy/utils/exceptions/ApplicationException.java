package pl.sdacademy.utils.exceptions;

/**
 * Created by Grzegorz on 2017-07-09.
 */
public class ApplicationException extends Exception {

    public ApplicationException(String message) {
        super(message);
    }
}
