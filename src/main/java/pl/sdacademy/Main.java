package pl.sdacademy;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pl.sdacademy.database.dbutils.DbManager;
import pl.sdacademy.utils.FillDatabase;
import pl.sdacademy.utils.FxmlUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class Main extends Application {

    public static final String MAIN_VIEW_FXML = "/pl/sdacademy/fxml/MainView.fxml";

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Pane pane = FxmlUtils.fxmlLoader(MAIN_VIEW_FXML);
        Scene scene = new Scene(pane);
        primaryStage.setTitle(FxmlUtils.getResourcesBundle().getString("title.application"));
        primaryStage.setScene(scene);
        primaryStage.show();

        DbManager.initDatabase();
        FillDatabase.fillDatabase();

    }
}


