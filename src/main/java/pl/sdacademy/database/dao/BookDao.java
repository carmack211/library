package pl.sdacademy.database.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import pl.sdacademy.database.models.Book;
import pl.sdacademy.utils.exceptions.ApplicationException;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Grzegorz on 2017-07-03.
 */
public class BookDao extends CommonDao {


    public BookDao() {
        super();
    }

    public List<String[]> findByTitle(String value) throws SQLException, ApplicationException {
        GenericRawResults<String []> where = getDao(Book.class)
                .queryRaw("SELECT * FROM books WHERE title = " + value );
        return where.getResults();
    }

    public List<Book> findByColumnNameAndValue (String columnName, String value)
            throws SQLException, ApplicationException {
        QueryBuilder<Book, Integer> queryBuilder = getQueryBuilder(Book.class);
        queryBuilder.where().eq(columnName, value);
        PreparedQuery<Book> prepare = queryBuilder.prepare();
        return getDao(Book.class).query(prepare);
    }

    public void deleteByColumnName(String columnName, int id) throws ApplicationException, SQLException {
        Dao<Book, Object> dao = getDao(Book.class);
        DeleteBuilder<Book, Object> deleteBuilder = dao.deleteBuilder();
        deleteBuilder.where().eq(columnName, id);
        dao.delete(deleteBuilder.prepare());
    }

}
