package pl.sdacademy.modelFx;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.sdacademy.database.dao.AuthorDao;
import pl.sdacademy.database.dao.BookDao;
import pl.sdacademy.database.dao.CategoryDao;
import pl.sdacademy.database.models.Author;
import pl.sdacademy.database.models.Book;
import pl.sdacademy.database.models.Category;
import pl.sdacademy.utils.converters.ConverterAuthor;
import pl.sdacademy.utils.converters.ConverterBook;
import pl.sdacademy.utils.converters.ConverterCategory;
import pl.sdacademy.utils.exceptions.ApplicationException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ListOfBooksModel {

    private ObservableList<BookFx> bookFxObservableList = FXCollections.observableArrayList();
    private ObservableList<AuthorFx> authorFxObservableList = FXCollections.observableArrayList();
    private ObservableList<CategoryFx> categoryFxObservableList = FXCollections.observableArrayList();

    //ustawiamy dwa obiekty, które będą przechowywały wybrane elementy
    //wybranie pozycji w comboBox zapisze daną pozycję
    private ObjectProperty<AuthorFx> authorFxObjectProperty = new SimpleObjectProperty<>();
    private ObjectProperty<CategoryFx> categoryFxObjectProperty = new SimpleObjectProperty<>();

    //lista przechowująca dane z bazy, dzięki czemu możemy filtrować dane w bookFxObservableList
    private List<BookFx> bookFxList = new ArrayList<>();

    public void init() throws ApplicationException {
        BookDao bookDao = new BookDao();
        List<Book> books = bookDao.queryForAll(Book.class);
        bookFxList.clear();
        books.forEach(e->{
            bookFxList.add(ConverterBook.convertToBookFx(e));
        });

        this.bookFxObservableList.setAll(bookFxList);

        initAuthor();
        initCategory();
    }

    public void filterListOfBooks(){
        if(getAuthorFxObjectProperty() != null && getCategoryFxObjectProperty() != null){
            filterPredicate(predicateAuthor().and(predicateCategory()));
        } else if(getAuthorFxObjectProperty()!=null){
            filterPredicate(predicateAuthor());
        }else if(getCategoryFxObjectProperty()!=null){
            filterPredicate(predicateCategory());
        }else {
            this.bookFxObservableList.setAll(this.bookFxList);
        }
    }

    private void initAuthor() throws ApplicationException {
        AuthorDao authorDao = new AuthorDao();
        List<Author> authors = authorDao.queryForAll(Author.class);
        authors.forEach(e->{
            authorFxObservableList.add(ConverterAuthor.convertToAuthorFx(e));
        });
    }

    private void initCategory() throws ApplicationException {
        CategoryDao categoryDao = new CategoryDao();
        List<Category> categories = categoryDao.queryForAll(Category.class);
        categories.forEach(e->{
            categoryFxObservableList.add(ConverterCategory.convertToCategoryFx(e));
        });
    }

    //warunek filtrowania
    private Predicate<BookFx> predicateCategory() {
        return bookFx -> bookFx.getCategoryFx().getId() == getCategoryFxObjectProperty().getId();
    }

    //warunek filtrowania
    private Predicate<BookFx> predicateAuthor(){
        return bookFx -> bookFx.getAuthorFx().getId() == getAuthorFxObjectProperty().getId();
    }


    private void filterPredicate(Predicate<BookFx> predicate) {
        List<BookFx> newList = bookFxList.stream().filter(predicate).collect(Collectors.toList());
        this.bookFxObservableList.setAll(newList);
    }


    public ObservableList<BookFx> getBookFxObservableList() {
        return bookFxObservableList;
    }

    public void setBookFxObservableList(ObservableList<BookFx> bookFxObservableList) {
        this.bookFxObservableList = bookFxObservableList;
    }

    public ObservableList<AuthorFx> getAuthorFxObservableList() {
        return authorFxObservableList;
    }

    public void setAuthorFxObservableList(ObservableList<AuthorFx> authorFxObservableList) {
        this.authorFxObservableList = authorFxObservableList;
    }

    public ObservableList<CategoryFx> getCategoryFxObservableList() {
        return categoryFxObservableList;
    }

    public void setCategoryFxObservableList(ObservableList<CategoryFx> categoryFxObservableList) {
        this.categoryFxObservableList = categoryFxObservableList;
    }

    public AuthorFx getAuthorFxObjectProperty() {
        return authorFxObjectProperty.get();
    }

    public ObjectProperty<AuthorFx> authorFxObjectPropertyProperty() {
        return authorFxObjectProperty;
    }

    public void setAuthorFxObjectProperty(AuthorFx authorFxObjectProperty) {
        this.authorFxObjectProperty.set(authorFxObjectProperty);
    }

    public CategoryFx getCategoryFxObjectProperty() {
        return categoryFxObjectProperty.get();
    }

    public ObjectProperty<CategoryFx> categoryFxObjectPropertyProperty() {
        return categoryFxObjectProperty;
    }

    public void setCategoryFxObjectProperty(CategoryFx categoryFxObjectProperty) {
        this.categoryFxObjectProperty.set(categoryFxObjectProperty);
    }

    public void deleteBook(BookFx bookFx) throws ApplicationException {
        BookDao bookDao = new BookDao();
        bookDao.deleteById(Book.class, bookFx.getId());
        init();
    }
}
